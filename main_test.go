package main

import (
	"fmt"
	"syscall/js"
	"testing"
)

// GOOS=js GOARCH=wasm go test -exec="$(go env GOROOT)/misc/wasm/go_js_wasm_exec"

func TestHelloWorld(t *testing.T) {
	// Hello function "wants" want (js.Value, []js.Value)
	this := js.ValueOf(nil)
	
	parameters := []js.Value{js.ValueOf("Bob"), js.ValueOf("Morane")}

	result := HelloWorld(this, parameters).(map[string]interface{})
	fmt.Println(result)

	expected_result := "👋 Hello World 🌍 Bob Morane"

	if result["message"] != expected_result {
		t.Error("Incorrect result, expected '"+ expected_result +"', got", result["message"])
	}
}

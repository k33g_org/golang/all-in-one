package main

import (
	"syscall/js"
)

func HelloWorld(this js.Value, args []js.Value) interface{} {
	// get the parameters
	firstName := args[0].String()
	lastName := args[1].String()

	return map[string]interface{}{
		"message": "👋 Hello World 🌍 " + firstName + " " + lastName,
		"author":  "@k33g_org",
	}
}

func main() {

	js.Global().Set("HelloWorld", js.FuncOf(HelloWorld))

	<-make(chan bool)
}

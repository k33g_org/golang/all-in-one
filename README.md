# All In One

## Initialize

### Environment setup

```bash
# go mod
go mod init gitlab.com/k33g_org/golang/all-in-one
# generate wasm_exec.js
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./js/
```

> The wasm_exec.js file is provided by Go to load your .wasm file into a Web page.

## Wasm compilation

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

## Serve index.html

```bash
python3 -m http.server
```

## Create a release

```bash
git tag -a v0.0.0 -m "my release"
git push origin v0.0.0
```